<?php
// CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, PATCH, DELETE');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: *');
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') exit;

$response = [];
if (!$_POST['n_index'] || $_POST['n_index'] == "null" || trim($_POST['n_index']) == "") {
    $data['result'] = [];
    $data['msg'] = "Índice vacío.";
    $data['ok'] = false;
} else {
    $n_index = $_POST['n_index'];
    $fibonacci  = [0, 1];

    for ($i = 2; $i <= $n_index; $i++) {
        $fibonacci[] = $fibonacci[$i - 1] + $fibonacci[$i - 2];
    }

    $data['result'] = $fibonacci[$n_index];
    $data['msg'] = "Se obtuvo el resultado.";
    $data['ok'] = true;
}

header("Content-type: application/json");
echo json_encode($data);
